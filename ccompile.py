import sys
import argparse
import subprocess
import os


def helper():
    """Prints usage information."""
    print("Usage: ccompiler [options] file1.c file2.c ...")
    print("Options:")
    print(" -n, --output <destinationFileName> : Choose the name of the compiled file (without extension)")
    return


def compile_files(file_list, output_name):
    """Compiles the given list of source files into an executable."""
    object_files = []
    for source_file in file_list:
        object_name = source_file.split(".")[0] + ".o"  # Replace ".c" with ".o"
        subprocess.run(["gcc", "-c", source_file, "-o", object_name])
        object_files.append(object_name)
    subprocess.run(["gcc"] + object_files + ["-o", output_name])
    for obj in object_files:
        os.remove(obj)  # Remove intermediate object files


def main():
    parser = argparse.ArgumentParser(description="A simple C compiler")
    parser.add_argument("files", metavar="file", nargs="+", help="Source files to compile")
    parser.add_argument("-n", "--output", metavar="output", help="Name of the compiled file (without extension)")
    args = parser.parse_args()

    if not args.files:
        print("Error: No input files provided.")
        parser.print_help()
        sys.exit(1)

    if args.output and "." in args.output:
        print("Error: Output file name cannot contain an extension.")
        sys.exit(1)

    output_name = args.output or "program"
    compile_files(args.files, output_name)


if __name__ == "__main__":
    main()
